Список використаних технологій:
HTML5;
SCSS-(медіа-запити, міксини, змінні);
BEM;
JS;
Принципи адаптивної верстки;

Налаштування збірки проекту за допомогою Gulp;
При збірці проєкту використовували наступні пакети, потрібного функціоналу для данного проекту:
gulp
gulp-sass
browser-sync
gulp-js-minify
gulp-uglify
gulp-clean-css
gulp-clean
gulp-concat
gulp-imagemin
gulp-autoprefixer

Склад учасників проекту:
Едуард Годун - eduard.hodun@gmail.com;
Владислав Листопад - @Listopadiiiik;

Які завдання виконував кожен із учасників:
Едуард Годун - Завдання 1;
Владислав Листопад - Завдання 2;

Посилання: https://eduardhodun.github.io/second-step-project-forkio/
